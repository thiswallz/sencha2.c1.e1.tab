Ext.define('Ejemplo1Tab.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
        'Ext.Video',
        'Ext.Img'
    ],
    config: {
        tabBarPosition: 'bottom',
        items: [
            {
                title: 'Form',
                iconCls: 'action',
                items: [
                    {
                        docked: 'top',
                        xtype: 'titlebar',
                        title: 'Titulo'
                    },{
                        xtype: 'formejemplo'
                    }
                ]
            },{
                title: 'HTML',
                iconCls: 'action',
                styleHtmlContent: true,
                scrollable: true,
                items: {
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Welcome to Sencha Touch 2'
                },
                html: [
                    "You've just generated a new Sencha Touch 2 project. What you're looking at right now is the ",
                    "contents of <a target='_blank' href=\"app/view/Main.js\">app/view/Main.js</a> - edit that file ",
                    "and refresh to change what's rendered here."
                ].join("")
            },
            {
                title: 'Form',
                iconCls: 'action',
                items: [
                    {
                        docked: 'top',
                        xtype: 'titlebar',
                        title: 'Titulo'
                    }, {
                    xtype: 'panel',
                    centered: true,
                    items: [{
                        xtype: 'image',
                        mode: 'image',
                        height: 64,
                        width: 64,
                        src: 'http://www.sencha.com/assets/images/sencha-avatar-64x64.png'
                    }]
                }]
            }
        ]
    }
});
