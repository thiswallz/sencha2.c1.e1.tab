Ext.define('Ejemplo1Tab.view.FormularioEjemplo', {
    extend: 'Ext.form.Panel',
    alias: "widget.formejemplo",    
    requires: [
        'Ext.TitleBar',
        'Ext.form.*',
        'Ext.field.*'
    ],
    config: {
        width: '100%',
        height: '100%',        
        items: [{
            xtype: 'fieldset',
            title: 'Titulo Fieldset',
            items: [{
                label : 'Nombre',
                xtype: 'textfield',
                placeHolder: 'Nombre',
                name: 'anexoNum'                
            },{
                label : 'Ok?',
                xtype: 'checkboxfield',
                name: 'ok'                
            },{
                xtype: 'radiofield',
                name : 'sexo',
                value: 'm',
                label: 'Masculino',
                checked: true
            },
            {
                xtype: 'radiofield',
                name : 'sexo',
                value: 'f',
                label: 'Femenino'
            }]
        },            
        {           
            xtype:'container',
            flex : 1,
            layout : {
                type : 'hbox',
                align: 'center'
            },
            items:[
            {
                xtype: 'button',
                ui : 'confirm',
                text: 'Boton',
                width: '90%',
                margin: 7,
                handler: function() {
                    Ext.Msg.alert('Info', "Boton Ejecutado!");
                }
            }]
        }]
    }
});
